## ISSUE CREATION TODOS:

Do these things when creating the issue.

- [ ] Title the issue "Pipeline Execution xx.xx Release Post Checklist"
- [ ] Assign the issue to the appropriate milestone
- [ ] Assign the issue to the PM and EM(s) for the team
- [ ] Add any [release posts already in flight](https://gitlab.com/dashboard/merge_requests?scope=all&state=opened&label_name[]=group%3A%3Apipeline%20execution&label_name[]=release%20post%20item) that are for this release
- [ ] :recycle: Create a retro thread for further iteration

## XX.XX Release Posts

| Issue | Feature Flag Issue | Engineer(s) | Issue Workflow Status | Docs URL | Release Post MR | Status |
| ---- | ---- | ---- | ---- | ---- | ---- | ---- |
| issue name / link | issue name / link | `@engineers` | `~workflow::` | link | link | status |

## Deprecation Notices

* Link to MR
* Link to MR

## Merged XX.XX Release Posts

| Issue | Feature Flag Issue | Engineer(s) | Issue Workflow Status | Docs URL | Release Post MR |
| ---- | ---- | ---- | ---- | ---- | ---- |
| issue name / link | issue name / link | `@engineers` | `~workflow::` | link | link |

## Move to next milestone

| Issue | Feature Flag Issue | Engineer(s) | Issue Workflow Status | Docs URL | Release Post MR |
| ---- | ---- | ---- | ---- | ---- | ---- |
| issue name / link | issue name / link | `@engineers` | `~workflow::` | link | link |

## ISSUE CLOSURE TODOS (Must be done before the 15th of the month):
- [ ] Make sure all release post items that have made it are merged.
- [ ] Open an issue for the next milestone and copy over anything in the Move to next milestone section
- [ ] Update the mielstone on MRs moved over.
- [ ] :tada: Celebrate!

## Links:

* [List of Release Post Merge Requests](https://gitlab.com/dashboard/merge_requests?scope=all&state=opened&label_name[]=group%3A%3Apipeline%20execution&label_name[]=release%20post%20item)
* [Issues with `release post item` label](https://gitlab.com/groups/gitlab-org/-/issues?sort=created_date&state=opened&label_name[]=group::pipeline+execution&label_name[]=release+post+item)

/label ~"team activities" ~section::ops ~devops::verify ~"group::pipeline execution"
/assign '@em' '@em' '@pm'
/milestone '%xx.xx'

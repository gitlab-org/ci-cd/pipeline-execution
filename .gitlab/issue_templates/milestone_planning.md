## TODO - how to create this item

- [ ] Title this issue `Pipeline Execution X.Y Planning issue`
- [ ] Replace <milestone> with the milestone and <last_milestone> with previous milestone values in glql blocks
- [ ] Update Goals and theme for the issue. These roll up to larger OKRs the team is working on or large initiatives.
- [ ] Add ~Verify::P1 issues Product would like to prioritize for release, these should contribute to the goals and theme
- [ ] Add ~Verify::P2 issues that are secondary / Stretch goals or would further contribute to the theme and OKRs
- [ ] Identify any Security, Infrastructure, Bug etc. issues that should prioritized per [Global Prioritization](https://about.gitlab.com/handbook/product/product-processes/#prioritization)
- [ ] Delete this todo list

## What's the focus, where can I find issue candidates for the milestone?

### Issue Boards
[PE workflow board](https://gitlab.com/groups/gitlab-org/-/boards/1372896) | [Issues by Team Member](https://gitlab.com/groups/gitlab-org/-/boards/1486481) 
 
### Recordings :movie_camera: 

* Refinement Kickoff - TBD
* Group Kickoff - TBD

### **Theme: Add the theme here** 

### Goals for the milestone (consider OKR and Yearly Alignment)

1. [ ]  
1. [ ]  

---

## Issues of note

This is a breakdown of the issues in the milestone. For more information about how we prioritize check out the [handbook page](https://about.gitlab.com/handbook/engineering/development/ops/verify/pipeline-execution/#prioritization-of-issues).


### Open Vulnerability issues

<details><summary>Click to expand</summary>

```glql
---
display: table
fields: title, assignees, weight, labels("bug::vulnerability"), labels("*SLO*"), due, labels("*end"),  labels("Verify::*"), labels("type::*"), milestone, labels("workflow::*"), labels
---
group = "gitlab-org" and label = "group::pipeline execution" and label = "security" and opened = true
```

</details>

### Open Infradev issues

<details><summary>Click to expand</summary>

```glql
---
display: table
fields: title, assignees, weight, labels("*end"), labels("*SLO*"), labels("Verify::*"), labels("type::*"), milestone, labels("workflow::*"), labels
---
group = "gitlab-org" and label = "group::pipeline execution" and label = "infradev" and opened = true
```

</details>

### Candidate Non-Engineering Time

<details><summary>Click to expand</summary>

```glql
---
display: table
fields: title, assignees, weight, labels("*end"), labels("Verify::*"), labels("type::*"), milestone, labels("workflow::*"), labels
---
group = "gitlab-org" and label = "group::pipeline execution" and label != "Engineering Time" and label = "candidate::<milestone>" and opened = true
```

</details>



### Candidate Engineering Time

<details><summary>Click to expand</summary>

```glql
---
display: table
fields: title, assignees, weight, labels("*end"), labels("type::*"), milestone, labels("workflow::*"), labels
---
group = "gitlab-org" and label = "group::pipeline execution" and label = "Engineering Time" and label = "candidate::<milestone>" and opened = true
```

</details>



### Potential Carry-forward Issues

<details><summary>Click to expand</summary>

```glql
---
display: table
fields: title, assignees, weight, labels("*end"), labels("type::*"), labels("Engineering Time", "Deliverable", "Stretch", "Spike"), labels("workflow::*"), milestone, labels
---
group = "gitlab-org" and label = "group::pipeline execution" and milestone = "<last_milestone>" and opened = true
```

</details>



### Scheduled Issues

<details><summary>Click to expand</summary>

```glql
---
display: table
fields: title, assignees, weight, labels("*end"), labels("type::*"), labels("Engineering Time", "Deliverable", "Stretch", "Spike"), labels("workflow::*"), milestone, labels
---
group = "gitlab-org" and label = "group::pipeline execution" and milestone = "<milestone>" and opened = true
```

</details>



## Planning Tasks

<details><summary>Click to expand</summary>

* [ ] PM updates list of candidate Issues (Non-Engineering Time)
* [ ] EM updates list of candidate Engineering Time Issues
* [ ] PM describes how this milestone aligns with the company's OKRs
* [ ] By 2 weeks before previous milestone cut-off EM tags Engineers in planning issue
* [ ] By Friday, 1 week before previous milestone cut-off Engineers complete planning task lists.
* [ ] By Monday of release week - EM reviews `~deliverable` and `~stretch` labels to assigned to issues.

</details>



Reference for [How We Plan](https://about.gitlab.com/handbook/engineering/development/ops/verify/pipeline-execution/#planning)

/label ~"Planning Issue" ~type::ignore ~"group::pipeline execution"
/assign @rutshah @carolinesimpson 
